import 'package:flutterdbapp/models/Company.dart';
import 'package:flutterdbapp/utils/Database.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class CompanyDBHelper {
  ///IT HOLDS THE TABLE CREATE QUERY
  final String tableName = 'Company';

  String getTableCreateQuery() {
    return '''
          CREATE TABLE IF NOT EXISTS $tableName(
          Id INTEGER PRIMARY KEY AUTOINCREMENT,
          Name TEXT,
          CustomerNo TEXT,
          TenantId INTEGER,
          DefaultBillAdd TEXT,
          Type TEXT,
          SalesRep TEXT,
          CurrencyCode TEXT,
          CreditLimit REAL,
          Balance REAL,
          TotalBalance REAL
          )
          ''';
  }

  Future<List<Company>> fetchCompanies() async {
    try {
      var companyData = List<Company>();
      String url = 'http://192.168.1.168:85/api/v1.0/GetCompanies';

      print('$url');
      http.Client client = http.Client();
      final response = await client.get(url, headers: {
        "token":
            "hNPYz3Lo3vpUUf4izGb5_ae3twLXAiKSUDnVGpoHnMzpA6iiOudqxSsLMp7ApYd0fTg19PiPT1SUBWYcKTtDw2DGuZ2DesSbJ4BDy6wRfRJQ2KrO-mOHiFvowSTg7pTPAsTbk0JvBGCe5jFaNfetYhCo-_L8tAPSNMMmprRDlVDmTMu4tlUU-_xEem4O-fzqdINAQTvCHfLtTFT__frz4Ol-0LOkZkgSuKObEQedo2536a3ywpRKR9nlGx6G869_jojQheZ3ouWIuv9l3R7iAYYLX2DFFKSnKmz3DWvn5Xow1I2rcctiYY1DyTUlAcIKyGlXF4h6ktTSv_fwqkIbKpFmVY4riEiwPdICYn_ySkPaBAMk-gtBB70_y2usg6mXSKxQYEdV2palY2btTxJkwfA9TXKIQ_Rbjy45hT0Yht5CaM4KiLkj7tFZQ4NyCBu417iaPRJfRD40PLXHYWHbtB3cFWeTYBmTNJgVXIi87HfkPMnt"
      }).timeout(Duration(seconds: 20));
      if (response.statusCode == 200) {
        var data = json.decode(response.body);
        companyData =
            data.map<Company>((json) => Company.fromJson(json)).toList();
      }
      return companyData;
    } catch (e) {
      print('Error inside fetchCompanies Fn ');
      print(e);
      throw Future.error(e);
    }
  }

  Future newCompany(Company company) async {
    final db = await DBProvider.db.database;
    var res = await db.insert('Company', company.toJson());
    return res;
  }

  String getFormattedString(String val) {
    return val != null ? val.replaceAll('"', ' ') : '';
  }

  Future addCompanies(List<Company> companies) async {
    try {
      final db = await DBProvider.db.database;
      print('Inside addCompanies after database connection received!');
      String values = "";
      for (var i = 0; i < companies.length; i++) {
        Company singleCompany = companies[i];

        double totalBal = singleCompany.totalBalance != null
            ? singleCompany.totalBalance
            : 0.0;
        values +=
//          '''( "${getFormattedString(singleCompany.name)}", "${getFormattedString(singleCompany.customerNo)}", ${singleCompany.tenantId}, "${getFormattedString(singleCompany.currencyCode)}", ${singleCompany.creditLimit}, ${singleCompany.balance}, "${getFormattedString(singleCompany.defaultBillAdd)}", "${getFormattedString(singleCompany.type)}", "${getFormattedString(singleCompany.salesRep)}", $totalBal )  , ''';
            '''( "${singleCompany.name}", "${singleCompany.customerNo}", ${singleCompany.tenantId}, "${singleCompany.currencyCode}", ${singleCompany.creditLimit}, ${singleCompany.balance}, "${singleCompany.defaultBillAdd}", "${singleCompany.type}", "${singleCompany.salesRep}", $totalBal )  , ''';
      }
      values = values.substring(0, values.lastIndexOf(',') - 1);

//    companies.forEach((singleCompany) {
//      double totalBal =
//          singleCompany.totalBalance != null ? singleCompany.totalBalance : 0.0;
//      values +=
//          '''( "${singleCompany.name}", "${singleCompany.customerNo}", ${singleCompany.tenantId}, "${singleCompany.currencyCode}", ${singleCompany.creditLimit}, ${singleCompany.balance}, "${singleCompany.defaultBillAdd}", "${singleCompany.type}", "${singleCompany.salesRep}", $totalBal ),''';
//    });
//        '( "Company 1", "Cust_1", 1, "INR", 10.0, 20.0, "DBA_1", "type_1", "salesRep_1", 30.0 ),
//        ( "Company 2", "Cust_2", 2, "USD", 20.0, 40.0, "DBA_2", "type_2", "salesRep_2", 60.0 )';

      var res = await db.rawInsert(''' 
    INSERT INTO Company ( name, customerNo, tenantId, currencyCode, creditLimit, balance, defaultBillAdd, type, salesRep, totalBalance ) 
          VALUES $values  
    ''');

      return res;
    } catch (e) {
      print('Error inside addCompanies');
      print(e);
      throw Future.error(e);
    }
  }

  Future<List<Company>> getAllCompanies() async {
    final db = await DBProvider.db.database;
    var res = await db.query("Company");
    List<Company> list =
        res.isNotEmpty ? res.map((c) => Company.fromJson(c)).toList() : [];
    return list;
  }
}
