import 'package:flutterdbapp/models/Product.dart';
import 'package:flutterdbapp/utils/Database.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ProductDBHelper {
  ///IT HOLDS THE TABLE CREATE QUERY
  final String tableName = 'Product';

  ///RETURNS THE CREATE TABLE QUERY FOR THE PRODUCT
  ///ANY NEW FIELDS ADD, UPDATE OR DELETE IN LOCAL DATABASE MUST BE DONE HERE
  String getTableCreateQuery() {
    return '''
          CREATE TABLE IF NOT EXISTS $tableName(
                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                    ProductCode TEXT,
                    Description TEXT,
                    BaseUnit TEXT,
                    IsTaxable TEXT,
                    Status TEXT,
                    ProductCategory TEXT,
                    BasePrice REAL,
                    Weight REAL,
                    UPCCode TEXT,
                    CreatedDate TEXT,
                    UpdatedDate TEXT
          )
          ''';
  }

  Future<List<Product>> fetchProducts() async {
    try {
      var productData = List<Product>();
      String url = 'http://192.168.1.168:85/api/v1.0/GetProducts';
      print('$url');
      http.Client client = http.Client();
      final response = await client.get(url, headers: {
        "token":
            "hNPYz3Lo3vpUUf4izGb5_ae3twLXAiKSUDnVGpoHnMzpA6iiOudqxSsLMp7ApYd0fTg19PiPT1SUBWYcKTtDw2DGuZ2DesSbJ4BDy6wRfRJQ2KrO-mOHiFvowSTg7pTPAsTbk0JvBGCe5jFaNfetYhCo-_L8tAPSNMMmprRDlVDmTMu4tlUU-_xEem4O-fzqdINAQTvCHfLtTFT__frz4Ol-0LOkZkgSuKObEQedo2536a3ywpRKR9nlGx6G869_jojQheZ3ouWIuv9l3R7iAYYLX2DFFKSnKmz3DWvn5Xow1I2rcctiYY1DyTUlAcIKyGlXF4h6ktTSv_fwqkIbKpFmVY4riEiwPdICYn_ySkPaBAMk-gtBB70_y2usg6mXSKxQYEdV2palY2btTxJkwfA9TXKIQ_Rbjy45hT0Yht5CaM4KiLkj7tFZQ4NyCBu417iaPRJfRD40PLXHYWHbtB3cFWeTYBmTNJgVXIi87HfkPMnt",
      }).timeout(Duration(seconds: 20));

      if (response.statusCode == 200) {
        var data = json.decode(response.body);

        productData =
            data.map<Product>((json) => Product.fromJson(json)).toList();
      }
      return productData;
    } catch (e) {
      print('Error inside fetchProducts Fn ');
      print(e);
      throw Future.error(e);
    }
  }

  Future newProduct(Product product) async {
    final db = await DBProvider.db.database;
    var res = await db.insert('$tableName', product.toJson());
    return res;
  }

  String getFormattedString(String val) {
    return val != null ? val.replaceAll('"', ' ') : '';
  }

  Future addProducts(List<Product> products) async {
    try {
      final db = await DBProvider.db.database;
      print('Inside addProducts after database connection received!');
      String values = "";
      for (var i = 0; i < products.length; i++) {
        Product singleProduct = products[i];
        values += '''(
        "${getFormattedString(singleProduct.ProductCode)}" , "${getFormattedString(singleProduct.Description)}", "${getFormattedString(singleProduct.BaseUnit)}" , "${getFormattedString(singleProduct.IsTaxable)}" , "${getFormattedString(singleProduct.Status)}" , "${getFormattedString(singleProduct.ProductCategory)}" , ${singleProduct.BasePrice} , ${singleProduct.Weight}, "${getFormattedString(singleProduct.UPCCode)}", "${getFormattedString(singleProduct.CreatedDate)}" , "${getFormattedString(singleProduct.UpdatedDate)}"  )  , ''';
//         "${singleProduct.ProductCode}" , "${singleProduct.Description}", "${singleProduct.BaseUnit}" , "${singleProduct.IsTaxable}" , "${singleProduct.Status}" , "${singleProduct.ProductCategory}" , ${singleProduct.BasePrice} , ${singleProduct.Weight}, "${singleProduct.UPCCode}", "${singleProduct.CreatedDate}" , "${singleProduct.UpdatedDate}"  )  , ''';
      }
      values = values.substring(0, values.lastIndexOf(',') - 1);

      var res = await db.rawInsert(''' 
    INSERT INTO $tableName (  ProductCode , Description, BaseUnit , IsTaxable , Status , ProductCategory , BasePrice , Weight, UPCCode, CreatedDate , UpdatedDate ) 
          VALUES $values  
    ''');

      return res;
    } catch (e) {
      print('Error inside addProducts');
      print(e);
//      return 'Unable to insert data into DB for Products';
      throw Future.error(e);
    }
  }

  Future<List<Product>> getAllProducts() async {
    final db = await DBProvider.db.database;
    var res = await db.query('$tableName');
    List<Product> list =
        res.isNotEmpty ? res.map((c) => Product.fromJson(c)).toList() : [];
    return list;
  }
}
