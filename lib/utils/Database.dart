import 'dart:convert';
import 'dart:async';
import 'package:flutterdbapp/utils/Helper/CompanyDBHelper.dart';
import 'package:flutterdbapp/utils/Helper/ProductDBHelper.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      print('Database ALready Exists');
      return _database;
    } else {
      print('Need to Initialize Database ');
    }
    _database = await initDB();
    return _database;
  }

  initDB() async {
    return await openDatabase(
      join(
        await getDatabasesPath(),
        'testDemo.db',
      ),
      onCreate: (db, version) async {
        print('Inside Oncreate Database!');

        ///CREATING THE COMPANY_TABLE
        await db.execute(CompanyDBHelper().getTableCreateQuery());
        print('Company Table created');

        ///CREATING THE PRODUCT_TABLE
        await db.execute(ProductDBHelper().getTableCreateQuery());
        print('Product Table created');
      },
      version: 1,
    );
  }
}
