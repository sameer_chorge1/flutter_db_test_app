import 'package:flutterdbapp/models/Product.dart';

import 'package:flutter/material.dart';
import 'package:flutterdbapp/utils/Database.dart';
import 'package:flutterdbapp/utils/Helper/ProductDBHelper.dart';

class ProductScreenPage extends StatefulWidget {
  @override
  _ProductScreenPageState createState() => _ProductScreenPageState();
}

class _ProductScreenPageState extends State<ProductScreenPage> {
  ///HOLDS ALL THE PRODUCTS LIST FOR SHOWING ON UI
  List<Product> _products;

  ///HOLDS PRODUCT DB HELPER FOR FETCHING FROM THE LOCAL_DATABASE
  ProductDBHelper _productDBHelper;
  @override
  void initState() {
    super.initState();
    _products = List<Product>();
    _productDBHelper = ProductDBHelper();
    fetchAllProducts();
  }

  ///FETCHES ALL THE PRODUCTS
  void fetchAllProducts() {
    try {
      this.setState(() {
        _products.clear();
      });
      print('fetchAllProducts called');
      _productDBHelper.getAllProducts().then(
            (productsRes) => {
              print('productsRes'),
              if (productsRes.length > 0)
                this.setState(() {
                  _products.addAll(productsRes);
                }),
            },
          );
    } catch (e) {
      print('Error indide productsRes function ');
      print(e);
    }
  }

  ///ADDS SINGLE DUMMY PRODUCT TO THE LOCAL_DB
  void handleNewProductInsert() {
    try {
      Product _product = Product(
        ProductCode: 'P01, tehre',
        Description: 'P01_DESC',
        Status: 'ENABLED',
        ProductCategory: 'ALL',
        BasePrice: 10.0,
        BaseUnit: 'EUR',
        Weight: 10.0,
        IsTaxable: 'YES',
        UPCCode: '12345678',
        CreatedDate: 'TODAY_CREATED_AT_SINGLE',
        UpdatedDate: 'TODAY_UPDATED_AT_SINGLE',
      );

      _productDBHelper.newProduct(_product).then(
            (value) => {
              print('Created Product res '),
              print(value),
              this.fetchAllProducts(),
            },
          );
    } catch (e) {
      print('Error inside handleNewProductInsert function ');
      print(e);
    }
  }

  ///DELETES ALL THE TABLE DATA
  void handleDeleteTableData() {
    print('Inside FN handleDeleteTableData for product');
    try {
      DBProvider.db.database.then((db) => {
            db.delete(_productDBHelper.tableName).then(
                  (deleteRes) => {
                    print('deleteRes'),
                    print(deleteRes),
                    this.fetchAllProducts(),
                  },
                ),
          });
    } catch (e) {
      print('Error inside handleDeleteTableData  for product');
      print(e);
    }
  }

  ///HANDLES MULTIPLE DUMMY PRODUCTS INSERT
  void handleMultipleProductsInsert() {
    try {
      List<Product> _productsList = [
        Product(
          ProductCode: 'P02',
          Description: 'P02_DESC',
          Status: 'ENABLED',
          ProductCategory: 'ALL',
          BasePrice: 10.0,
          BaseUnit: 'EUR',
          Weight: 10.0,
          IsTaxable: 'YES',
          UPCCode: '12345678',
          CreatedDate: 'TODAY_CREATED_AT_MULTIPLE_1',
          UpdatedDate: 'TODAY_UPDATED_AT_MULTIPLE_1',
        ),
        Product(
          ProductCode: 'P03',
          Description: 'P03_DESC',
          Status: 'ENABLED',
          ProductCategory: 'ALL',
          BasePrice: 10.0,
          BaseUnit: 'EUR',
          Weight: 10.0,
          IsTaxable: 'YES',
          UPCCode: '12345678',
          CreatedDate: 'TODAY_CREATED_AT_MULTIPLE_2',
          UpdatedDate: 'TODAY_UPDATED_AT_MULTIPLE_2',
        )
      ];
      _productDBHelper.addProducts(_productsList).then(
            (value) => {
              print('multiple Dummy Products Insert res '),
              print(value),
              this.fetchAllProducts(),
            },
          );
    } catch (e) {
      print('Error inside handleMultipleProductsInsert function ');
      print(e);
    }
  }

  ///IT FETCHES PRODUCTS DATA FROM API AND THEN INSERT IT TO THE LOCAL DB
  void handleAPIProductDataInsert() {
    try {
      _productDBHelper.fetchProducts().then((apiProductsRes) => {
            print('fetchProducts Response received from API'),
            if (apiProductsRes.length > 0)
              {
                _productDBHelper.addProducts(apiProductsRes).then(
                      (value) => {
                        print('Created multiple Products res received'),
                        this.fetchAllProducts(),
                      },
                    ),
              }
            else
              {
                print('No Products list received from api call'),
              }
          });
    } catch (e) {
      print('Error inside handleAPIProductDataInsert function ');
      print(e);
    }
  }

  ///HANDLES ALL THE RAISED BUTTONS FOR ON_PRESSED FUNCTIONS AND LABELS_TEXT
  Widget getRaisedButton({
    String label,
    Function onPressedFn,
  }) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: RaisedButton(
          onPressed: () {
            onPressedFn();
          },
          child: Text('$label'),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product Screen'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                  child: Center(
                      child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                child: Text(
                  'PRODUCTS CRUD',
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
              ))),
            ],
          ),
          Row(
            children: <Widget>[
              getRaisedButton(
                label: 'Dummy Data Insert',
                onPressedFn: handleNewProductInsert,
              ),
              getRaisedButton(
                label: 'Fetch Products Data',
                onPressedFn: fetchAllProducts,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              getRaisedButton(
                label: 'DELETE TABLE DATA',
                onPressedFn: handleDeleteTableData,
              ),
              getRaisedButton(
                label: 'ADD MULTIPLE PRODUCTS',
                onPressedFn: handleMultipleProductsInsert,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              getRaisedButton(
                label: 'Insert_API_Products',
                onPressedFn: handleAPIProductDataInsert,
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: _products.length,
              itemBuilder: (context, position) {
                return Column(
                  children: <Widget>[
                    Divider(
                      color: Colors.teal,
                      thickness: 2.0,
                    ),
                    Text('${_products[position].Id}'),
                    Text('${_products[position].ProductCode}'),
                    Text('${_products[position].Description}'),
                    Text('${_products[position].Status}'),
                    Text('${_products[position].ProductCategory}'),
                    Text('${_products[position].BasePrice}'),
                    Text('${_products[position].BaseUnit}'),
                    Text('${_products[position].Weight}'),
                    Text('${_products[position].IsTaxable}'),
                    Text('${_products[position].UPCCode}'),
                    Text('${_products[position].CreatedDate}'),
                    Text('${_products[position].UpdatedDate}'),
//                    Text('${_products[position].Image}'),
                    Divider(
                      color: Colors.teal,
                      thickness: 2.0,
                    ),
                  ],
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
