import 'dart:convert';
import 'package:background_fetch/background_fetch.dart';
import 'package:flutterdbapp/Screens/CompanyScreen.dart';
import 'package:flutterdbapp/Screens/ProductScreen.dart';
import 'package:flutterdbapp/models/Product.dart';
import 'package:flutterdbapp/utils/Helper/CompanyDBHelper.dart';
import 'package:flutterdbapp/utils/Helper/ProductDBHelper.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutterdbapp/models/Company.dart';
import 'package:flutterdbapp/utils/Database.dart';
import 'package:flutterdbapp/utils/Helper/Helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

const EVENTS_KEY = "fetch_events";

class HomePage extends StatefulWidget {
  final List<Product> apiProductsRes;
  HomePage({
    @required this.apiProductsRes,
  });
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Color _greyOutColor = Color(0xff181a25);
  bool _enabled = true;
  int _status = 0;
  List<String> _events = [];

  ProductDBHelper _productDBHelper = ProductDBHelper();
  @override
  void initState() {
//    initPlatformState();
    super.initState();
//    scheduleFetchProductTask();
  }

  ///BACKGROUND_FETCH NEW CHANGES START
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Load persisted fetch events from SharedPreferences
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String json = prefs.getString(EVENTS_KEY);
    if (json != null) {
      setState(() {
        _events = jsonDecode(json).cast<String>();
      });
    }

    // Configure BackgroundFetch.
    BackgroundFetch.configure(
            BackgroundFetchConfig(
              minimumFetchInterval: 15,
              forceAlarmManager: false,
              stopOnTerminate: false,
              startOnBoot: true,
              enableHeadless: true,
              requiresBatteryNotLow: false,
              requiresCharging: false,
              requiresStorageNotLow: false,
              requiresDeviceIdle: false,
              requiredNetworkType: NetworkType.ANY,
            ),
            _onBackgroundFetch)
        .then((int status) {
      print('[BackgroundFetch] configure success: $status');
      setState(() {
        _status = status;
      });
    }).catchError((e) {
      print('[BackgroundFetch] configure ERROR: $e');
      setState(() {
        _status = e;
      });
    });

    // Schedule a "one-shot" custom-task in 10000ms.
    // These are fairly reliable on Android (particularly with forceAlarmManager) but not iOS,
    // where device must be powered (and delay will be throttled by the OS).

    // Optionally query the current BackgroundFetch status.
    int status = await BackgroundFetch.status;
    setState(() {
      _status = status;
    });

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }

  void scheduleFetchProductTask() {
    try {
//    adb shell cmd jobscheduler run -f com.example.flutterdbapp 999
//adb shell cmd jobscheduler run -f com.example.flutterdbapp.event.com.transistorsoft.fetchProducts 999
      BackgroundFetch.scheduleTask(TaskConfig(
        delay: 0,
        taskId: "com.transistorsoft.fetchProducts",
        periodic: false,
        forceAlarmManager: true,
        stopOnTerminate: false,
        enableHeadless: true,
        requiredNetworkType: NetworkType.ANY,
      ));
    } catch (e) {
      print('Error inside scheduleFetchProductTask FN ');
      print(e);
    }
  }

  void _onBackgroundFetch(String taskId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    DateTime timestamp = new DateTime.now();
    // This is the fetch-event callback.
    print("[BackgroundFetch] Event received: $taskId");
    setState(() {
      _events.insert(0, "$taskId@${timestamp.toString()}");
    });
    // Persist fetch events in SharedPreferences
    prefs.setString(EVENTS_KEY, jsonEncode(_events));

    if (taskId == "flutter_background_fetch") {
      // Schedule a one-shot task when fetch event received (for testing).
      BackgroundFetch.finish(taskId);
    } else if (taskId == "com.transistorsoft.fetchProducts") {
      print('Inside _onBackgroundFetch for taskID: $taskId ');

      handleAPIProductDataInsert(
        taskId: taskId,
      );
    } else {
      print(
          'Finishing the task which is not handled inside _onBackgroundFetch ');
      BackgroundFetch.finish(taskId);
    }

    ///HERE BackgroundFetch.finish(taskId); IS COMMENTED AND WILL END AFTER ALL THE PRODUCTS FETCHED
    // IMPORTANT:  You must signal completion of your fetch task or the OS can punish your app
    // for taking too long in the background.
//    BackgroundFetch.finish(taskId);
  }

  void handleAPIProductDataInsert({taskId}) {
    print('Inside handleAPIProductDataInsert Fn with taskId : $taskId ');
    try {
      ///FROM HERE UNCOMMENT THE CODE
      if (widget.apiProductsRes.length > 0) {
        DBProvider.db.database.then((db) => {
              db.delete(_productDBHelper.tableName).then(
                    (deleteRes) => {
                      print('deleteRes'),
                      print(deleteRes),
                      print('Proceeding to insert Product data into localDB'),
                      _productDBHelper.addProducts(widget.apiProductsRes).then(
                            (value) => {
                              print(
                                  'Products Insert into Local Database is successful!'),
                              handleBackgroundFetchFinish(taskId: taskId),
                              print('ALl Data Inserted to the local Database'),
                            },
                          ),
                    },
                  ),
            });
      } else {
        handleBackgroundFetchFinish(taskId: taskId);
        handleErrorCallback(
          reason: 'No products list received from api call',
        );
      }

      ///TILL HERE UNCOMMENT THE CODE
    } catch (e) {
      print('Error inside handleAPIProductDataInsert function ');

      print(e);
      handleBackgroundFetchFinish(taskId: taskId);
      handleErrorCallback(
        reason: 'Error Occured while API Product data fetch or insertion',
      );
    }
  }

  ///Handling the errors
  void handleErrorCallback({String reason}) {
    print('inside handleErrorCallback Error reason: $reason');
    print(
        'Due to error navigating to the Home Screen without inserting data into local tables');
//    this.setState(() {
//      progressIndicatorVal = 1.0;
//      showSplashScreen = false;
//    });
  }

  void handleBackgroundFetchFinish({
    taskId,
  }) {
    try {
      print(
          'Inside handleBackgroundFetchFinish Fn for finishing the task for taskID: $taskId');
      if (taskId != null)
        BackgroundFetch.finish(taskId);
      else
        print('TaskID not present for closing Finishing the task');
    } catch (e) {
      print('Error inside handleBackgroundFetchFinish fn ');
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Home Screen'),
        ),
        drawer: Theme(
          data: Theme.of(context).copyWith(canvasColor: _greyOutColor),
          child: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Container(
                  height: 100.0,
                  child: DrawerHeader(
                    margin: EdgeInsets.all(0.0),
                    padding: EdgeInsets.only(
                      left: 20.0,
                      top: 10.0,
                      right: 10.0,
                      bottom: 10.0,
                    ),
                    decoration: BoxDecoration(
                      color: _greyOutColor, //Color(0xffB1B1B1),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Custom User Data',
                          style: TextStyle(color: Colors.white),
                        )),
                      ],
                    ),
                  ),
                ),
                Divider(
                  thickness: 2.0,
                  color: Colors.white,
                ),
                Ink(
                  color: _greyOutColor,
                  child: new ListTile(
                    leading: Icon(Icons.people, color: Colors.white),
                    title: Text(
                      'Company CRUD',
                      style: TextStyle(color: Colors.white),
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new CompanyScreenPage()));
                    },
                  ),
                ),
                Ink(
                  color: _greyOutColor,
                  child: new ListTile(
                    leading: Icon(Icons.people, color: Colors.white),
                    title: Text(
                      'Product CRUD',
                      style: TextStyle(color: Colors.white),
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new ProductScreenPage()));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        body: Container(
          child: Center(
              child: Text(
            'HOME PAGE',
            style: TextStyle(
              color: Color(0xff626DFE),
              fontWeight: FontWeight.bold,
              fontSize: 40.0,
            ),
          )),
        ),
      ),
    );
  }
}
