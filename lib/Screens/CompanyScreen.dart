import 'dart:convert';
import 'package:flutterdbapp/utils/Helper/CompanyDBHelper.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutterdbapp/models/Company.dart';
import 'package:flutterdbapp/utils/Database.dart';
import 'package:flutterdbapp/utils/Helper/Helper.dart';

class CompanyScreenPage extends StatefulWidget {
  @override
  _CompanyScreenPageState createState() => _CompanyScreenPageState();
}

class _CompanyScreenPageState extends State<CompanyScreenPage> {
  List<Company> companies;
  CompanyDBHelper _companyDBHelper;
  @override
  void initState() {
    super.initState();
    companies = List<Company>();
    _companyDBHelper = CompanyDBHelper();
    fetchAllCompanies();
  }

  void fetchAllCompanies() {
    try {
      this.setState(() {
        companies.clear();
      });
      print('fetchAllCompanies called');
      _companyDBHelper.getAllCompanies().then(
            (companiesRes) => {
              print('companiesRes'),
              print(companiesRes),
              this.setState(() {
                companies.addAll(companiesRes);
              }),
            },
          );
    } catch (e) {
      print('Error indide fetchAllCompanies function ');
      print(e);
    }
  }

  void handleNewCompanyInsert() {
    try {
      Company company = Company(
        name: 'On,e',
        balance: 20.0,
        creditLimit: 9.0,
        customerNo: '01',
        currencyCode: 'INR',
        defaultBillAdd: 'TEst',
        salesRep: "OWN",
        tenantId: 01,
        totalBalance: 10.0,
        type: 'huedh',
      );
      _companyDBHelper.newCompany(company).then(
            (value) => {
              print('Created Company res '),
              print(value),
              this.fetchAllCompanies(),
            },
          );
    } catch (e) {
      print('Error inside handleNewCompanyInsert function ');
      print(e);
    }
  }

  void handleAPICompanyDataInsert() {
    try {
      _companyDBHelper.fetchCompanies().then((apiCompaniesRes) => {
            print('fetchCompanies Response received from API'),
            if (apiCompaniesRes.length > 0)
              {
                _companyDBHelper.addCompanies(apiCompaniesRes).then(
                      (value) => {
                        print('Created multiple Companies res received'),
                        this.fetchAllCompanies(),
                      },
                    ),
              }
            else
              {
                print('No companies list received from api call'),
              }
          });
    } catch (e) {
      print('Error inside handleAPICompanyDataInsert function ');
      print(e);
    }
  }

  void handleMultipleCompanyInsert() {
    try {
      List<Company> companies = [
        Company(
          name: 'One,',
          balance: 20.0,
          creditLimit: 9.0,
          customerNo: '01',
          currencyCode: 'INR',
          defaultBillAdd: 'TEst',
          salesRep: "OWN",
          tenantId: 01,
          totalBalance: 10.0,
          type: 'huedh',
        ),
        Company(
          name: "d'Cruz",
          balance: 20.0,
          creditLimit: 9.0,
          customerNo: '01',
          currencyCode: 'INR',
          defaultBillAdd: 'TEst',
          salesRep: "OWN",
          tenantId: 01,
          totalBalance: 10.0,
          type: 'huedh',
        )
      ];
      _companyDBHelper.addCompanies(companies).then(
            (value) => {
              print('Created multiple Companies res '),
              print(value),
              this.fetchAllCompanies(),
            },
          );
    } catch (e) {
      print('Error inside handleMultipleCompanyInsert function ');
      print(e);
    }
  }

  void handleDeleteTableData() {
    print('Inside FN handleDeleteTableData');
    try {
      DBProvider.db.database.then((db) => {
            db.delete('Company').then(
                  (deleteRes) => {
                    print('deleteRes'),
                    print(deleteRes),
                    this.fetchAllCompanies(),
                  },
                ),
          });
    } catch (e) {
      print('Error inside handleDeleteTableData ');
      print(e);
    }
  }

  ///HANDLES ALL THE RAISED BUTTONS FOR ON_PRESSED FUNCTIONS AND LABELS_TEXT
  Widget getRaisedButton({
    String label,
    Function onPressedFn,
  }) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: RaisedButton(
          onPressed: () {
            onPressedFn();
          },
          child: Text('$label'),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Screen'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                  child: Center(
                      child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                child: Text(
                  'COMPANIES CRUD',
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
              ))),
            ],
          ),
          Row(
            children: <Widget>[
              getRaisedButton(
                label: 'Dummy Data Insert',
                onPressedFn: handleNewCompanyInsert,
              ),
              getRaisedButton(
                label: 'Fetch Companies Data',
                onPressedFn: fetchAllCompanies,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              getRaisedButton(
                label: 'DELETE TABLE DATA',
                onPressedFn: handleDeleteTableData,
              ),
              getRaisedButton(
                label: 'ADD MULTIPLE COMPANIES',
                onPressedFn: handleMultipleCompanyInsert,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              getRaisedButton(
                label: 'Insert_API_Companies',
                onPressedFn: handleAPICompanyDataInsert,
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: companies.length,
              itemBuilder: (context, position) {
                return Column(
                  children: <Widget>[
                    Divider(
                      color: Colors.teal,
                      thickness: 2.0,
                    ),
                    Text(companies[position].id.toString()),
                    Text(companies[position].name),
                    Text(companies[position].balance.toString()),
                    Text(companies[position].creditLimit.toString()),
                    Text(companies[position].currencyCode),
                    Text(companies[position].defaultBillAdd),
                    Text(companies[position].tenantId.toString()),
                    Text(companies[position].type),
                    Text(companies[position].totalBalance.toString()),
                    Text(companies[position].balance.toString()),
                    Divider(
                      color: Colors.teal,
                      thickness: 2.0,
                    ),
                  ],
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
