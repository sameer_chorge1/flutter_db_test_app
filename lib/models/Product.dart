class Product {
  int Id;
  String ProductCode;
  String Description;
  String Status;
  String ProductCategory;
  double BasePrice;
  String BaseUnit;
  double Weight;
  String IsTaxable;
  String UPCCode;
  String CreatedDate;
  String UpdatedDate;
//  String Image;

  Product({
    this.Id,
    this.ProductCode,
    this.Description,
    this.Status,
    this.ProductCategory,
    this.BasePrice,
    this.BaseUnit,
    this.Weight,
    this.IsTaxable,
    this.UPCCode,
    this.CreatedDate,
    this.UpdatedDate,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      Id: json['Id'] as int,
      ProductCode: json['ProductCode'] as String,
      Description: json['Description'] as String,
      Status: json['Status'] as String,
      ProductCategory: json['ProductCategory'] as String,
      BasePrice: json['BasePrice'] as double,
      BaseUnit: json['BaseUnit'] as String,
      Weight: json['Weight'] as double,
      IsTaxable: json['IsTaxable'] as String,
      UPCCode: json['UPCCode'] as String,
      CreatedDate: json['CreatedDate'] as String,
      UpdatedDate: json['UpdatedDate'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'Id': Id,
      'ProductCode': ProductCode,
      'Description': Description,
      'Status': Status,
      'ProductCategory': ProductCategory,
      'BasePrice': BasePrice,
      'BaseUnit': BaseUnit,
      'Weight': Weight,
      'IsTaxable': IsTaxable,
      'UPCCode': UPCCode,
      'CreatedDate': CreatedDate,
      'UpdatedDate': UpdatedDate,
    };
  }
}
