// To parse this JSON data, do
//
//     final company = companyFromJson(jsonString);

import 'dart:convert';

Company companyFromJson(String str) => Company.fromJson(json.decode(str));

String companyToJson(Company data) => json.encode(data.toJson());

class Company {
  int id;
  String name;
  String customerNo;
  int tenantId;
  String currencyCode;
  double creditLimit;
  double balance;
  String defaultBillAdd;
  String type;
  String salesRep;
  double totalBalance;

  Company({
    this.id,
    this.name,
    this.customerNo,
    this.tenantId,
    this.currencyCode,
    this.creditLimit,
    this.balance,
    this.defaultBillAdd,
    this.type,
    this.salesRep,
    this.totalBalance,
  });

  factory Company.fromJson(Map<String, dynamic> json) => Company(
        id: json["Id"] as int,
        name: json["Name"] as String,
        customerNo: json["CustomerNo"] as String,
        tenantId: json["TenantId"] as int,
        currencyCode: json["CurrencyCode"] as String,
        creditLimit: json["CreditLimit"] as double,
        balance: json["Balance"] as double,
        defaultBillAdd: json["DefaultBillAdd"] as String,
        type: json["Type"] as String,
        salesRep: json["SalesRep"] as String,
        totalBalance: json["TotalBalance"] as double,
      );

  Map<String, dynamic> toJson() => {
        "Id": id,
        "Name": name,
        "CustomerNo": customerNo,
        "TenantId": tenantId,
        "CurrencyCode": currencyCode,
        "CreditLimit": creditLimit,
        "Balance": balance,
        "DefaultBillAdd": defaultBillAdd,
        "Type": type,
        "SalesRep": salesRep,
        "TotalBalance": totalBalance,
      };
}
