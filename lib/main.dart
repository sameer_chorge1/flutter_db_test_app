import 'dart:convert';
import 'dart:isolate';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutterdbapp/Screens/Home.dart';
import 'package:flutterdbapp/models/Product.dart';
import 'package:flutterdbapp/utils/Database.dart';
import 'package:flutterdbapp/utils/Helper/CompanyDBHelper.dart';
import 'package:flutterdbapp/utils/Helper/ProductDBHelper.dart';
import 'package:flutter/material.dart';
import 'package:background_fetch/background_fetch.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

const EVENTS_KEY = "fetch_events";
const FETCH_PRODUCTS_TASK_ID = "com.transistorsoft.fetchProducts";

CompanyDBHelper _companyDBHelper = CompanyDBHelper();
ProductDBHelper _productDBHelper = ProductDBHelper();

/// This "Headless Task" is run when app is terminated.
void backgroundFetchHeadlessTask(String taskId) async {
  print("[BackgroundFetch] Headless event received: $taskId");
  DateTime timestamp = DateTime.now();

  SharedPreferences prefs = await SharedPreferences.getInstance();

  // Read fetch_events from SharedPreferences
  List<String> events = [];
  String json = prefs.getString(EVENTS_KEY);
  if (json != null) {
    events = jsonDecode(json).cast<String>();
  }
  // Add new event.
  events.insert(0, "$taskId@$timestamp [Headless]");
  // Persist fetch events in SharedPreferences
  prefs.setString(EVENTS_KEY, jsonEncode(events));

//  BackgroundFetch.finish(taskId);
  print('Inside backgroundFetchHeadlessTask taskID: $taskId');
  if (taskId == 'flutter_background_fetch') {
    BackgroundFetch.finish(taskId);
  } else if (taskId == FETCH_PRODUCTS_TASK_ID) {
    print(
        'Inside backgroundFetchHeadlessTask Handling fetchProducts Background task');
//    scheduleFetchProductTask();
    BackgroundFetch.finish(taskId);
  } else {
    print(
        'Inside backgroundFetchHeadlessTask Finishing the TaskId : $taskId as no handler specified ');
    BackgroundFetch.finish(taskId);
  }
}

void scheduleFetchProductTask() {
  try {
//    adb shell cmd jobscheduler run -f com.example.flutterdbapp 999
//adb shell cmd jobscheduler run -f com.example.flutterdbapp.event.com.transistorsoft.fetchProducts 999
    BackgroundFetch.scheduleTask(TaskConfig(
      taskId: FETCH_PRODUCTS_TASK_ID,
      delay: 0,
      periodic: false,
      forceAlarmManager: true,
      stopOnTerminate: false,
      enableHeadless: true,
      requiredNetworkType: NetworkType.ANY,
    ));
  } catch (e) {
    print('Error inside scheduleFetchProductTask FN ');
    print(e);
  }
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyAPP());

//  runApp(MyAPP());

  // Register to receive BackgroundFetch events after app is terminated.
  // Requires {stopOnTerminate: false, enableHeadless: true}
//  BackgroundFetch.registerHeadlessTask(backgroundFetchHeadlessTask);
}

///THIS FUNCTION IS MAIN
Future<String> handleAPIProductDataInsert(String taskId
//      {
//    taskId,
////    ReceivePort port,
//  }
    ) async {
  try {
    if (taskId == null) {
      taskId = FETCH_PRODUCTS_TASK_ID;
    }
    print('Inside handleAPIProductDataInsert Fn with taskId : $taskId ');
    print('Database Object Present for performing CRUD operations ');
    List<Product> apiProductsRes = await _productDBHelper.fetchProducts();
    if (apiProductsRes != null && apiProductsRes.length > 0) {
      print('fetchProducts Response received from API ');

      var db = await DBProvider.db.database;
      if (db != null) {
        print('Proceeding to insert Product data into localDB');
        var addProductsRes = await _productDBHelper.addProducts(apiProductsRes);
        print('Products Insert into Local Database is successful!');
        print('addProductsRes Response $addProductsRes');
        handleBackgroundFetchFinishStatic(taskId: taskId);
        return 'success';
      } else {
        print('Database Object not found from the databse helper ');
        handleBackgroundFetchFinishStatic(taskId: taskId);
        handleErrorCallbackStatic(
          reason: 'DB_NOT_FOUND',
        );
        return 'DB_NOT_FOUND';
      }
    } else {
      handleBackgroundFetchFinishStatic(taskId: taskId);
      handleErrorCallbackStatic(
        reason: 'No products list received from api call',
      );
      return 'Error';
    }
  } catch (e) {
    print('Error inside handleAPIProductDataInsert function ');

    print(e);
    handleBackgroundFetchFinishStatic(taskId: taskId);
    handleErrorCallbackStatic(
      reason: 'Error Occured while API Product data fetch or insertion',
    );

//      return Future.value('Error Occured');
    return 'Error';
  }
}

void handleBackgroundFetchFinishStatic({
  taskId,
}) {
  try {
    print(
        'Inside handleBackgroundFetchFinish Fn for finishing the task for taskID: $taskId');
    if (taskId != null)
      BackgroundFetch.finish(taskId);
    else
      print('TaskID not present for closing Finishing the task');
  } catch (e) {
    print('Error inside handleBackgroundFetchFinish fn ');
    print(e);
  }
}

///Handling the errors
void handleErrorCallbackStatic({String reason}) {
  print(' inside Global Function handleErrorCallback Error reason: $reason');
  print(
      'Due to error navigating to the Home Screen without inserting data into local tables Inside Global Function');
//  this.setState(() {
//    progressIndicatorVal = 1.0;
//    showSplashScreen = false;
//  });
}

class MyAPP extends StatefulWidget {
  @override
  _MyAPPState createState() => _MyAPPState();
}

class _MyAPPState extends State<MyAPP> {
  bool showSplashScreen;
  double progressIndicatorVal;

  List<Product> _products;
  bool _enabled = true;
  int _status = 0;
  List<String> _events = [];

  ///TIMERS
  Duration _productsFetchMSTime;
  Duration _productsLocalDeleteMSTime;
  Duration _productsLocalInsertMSTime;

  @override
  void initState() {
    super.initState();
    initPlatformState();
//    _companyDBHelper = CompanyDBHelper();
//    _productDBHelper = ProductDBHelper();
    showSplashScreen = true;
    progressIndicatorVal = 0.0;
    _productsFetchMSTime = Duration(microseconds: 0);
    _productsLocalDeleteMSTime = Duration(microseconds: 0);
    _productsLocalInsertMSTime = Duration(microseconds: 0);
    _products = List<Product>();
//    if (showSplashScreen) handleAPICompanyDataInsert();
    if (showSplashScreen) scheduleFetchProductTask();
//    if (showSplashScreen) handleAPIProductDataInsert();
//    this.updateProgress(0.5),
  }

  ///BACKGROUND_FETCH NEW CHANGES START
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Load persisted fetch events from SharedPreferences
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String json = prefs.getString(EVENTS_KEY);
    if (json != null) {
      setState(() {
        _events = jsonDecode(json).cast<String>();
      });
    }

    // Configure BackgroundFetch.
    BackgroundFetch.configure(
            BackgroundFetchConfig(
              minimumFetchInterval: 15,
              forceAlarmManager: false,
              stopOnTerminate: false,
              startOnBoot: true,
              enableHeadless: true,
              requiresBatteryNotLow: false,
              requiresCharging: false,
              requiresStorageNotLow: false,
              requiresDeviceIdle: false,
              requiredNetworkType: NetworkType.ANY,
            ),
            _onBackgroundFetch)
        .then((int status) {
      print('[BackgroundFetch] configure success: $status');
      setState(() {
        _status = status;
      });
    }).catchError((e) {
      print('[BackgroundFetch] configure ERROR: $e');
      setState(() {
        _status = e;
      });
    });

    // Schedule a "one-shot" custom-task in 10000ms.
    // These are fairly reliable on Android (particularly with forceAlarmManager) but not iOS,
    // where device must be powered (and delay will be throttled by the OS).

    // Optionally query the current BackgroundFetch status.
    int status = await BackgroundFetch.status;
    setState(() {
      _status = status;
    });

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }

  void _onBackgroundFetch(String taskId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    DateTime timestamp = new DateTime.now();
    // This is the fetch-event callback.
    print("[BackgroundFetch] Event received: $taskId");
    setState(() {
      _events.insert(0, "$taskId@${timestamp.toString()}");
    });
    // Persist fetch events in SharedPreferences
    prefs.setString(EVENTS_KEY, jsonEncode(_events));

    if (taskId == "flutter_background_fetch") {
      // Schedule a one-shot task when fetch event received (for testing).
      BackgroundFetch.finish(taskId);
    } else if (taskId == FETCH_PRODUCTS_TASK_ID) {
      print('Inside _onBackgroundFetch for taskID: $taskId ');
      setState(() {
        showSplashScreen = false;
      });
      updateProgress(0.2);
      try {
        await compute(handleAPIProductDataInsert, '$taskId');
      } catch (e) {
        print(
            'Error while calling the compute on handleAPIProductDataInsert function ');
        print(e);
      }
    } else {
      print(
          'Finishing the task which is not handled inside _onBackgroundFetch ');
      BackgroundFetch.finish(taskId);
    }

    ///HERE BackgroundFetch.finish(taskId); IS COMMENTED AND WILL END AFTER ALL THE PRODUCTS FETCHED
    // IMPORTANT:  You must signal completion of your fetch task or the OS can punish your app
    // for taking too long in the background.
//    BackgroundFetch.finish(taskId);
  }

  Future loadIsolate({
    taskId,
//    databaseObj,
  }) async {
    print('Inside loadIsolate Fn ');
    try {
      if (taskId == FETCH_PRODUCTS_TASK_ID) {
        setState(() {
          showSplashScreen = false;
        });
        ReceivePort receivePort = ReceivePort();
//    await Isolate.spawn(isolateEntry, receivePort.sendPort);
        await Isolate.spawn(isolateEntry, receivePort.sendPort);
//            .then((value) => {
//                  print('Iside Isolate.spawn .then response '),
//                  print(value),
//                })
//            .catchError((err) {
//          print('Inside Isolate.spawn catch Error ');
//          print(err);
//        });

//      SendPort sendPort = await receivePort.first;

//    List message = await sendRecieve(
//        sendPort, "https://jsonplaceholder.typicode.com/comments");

//    setState(() => list = message);

      } else {
        print(
            'Inside loadIsolate Else as current Task ID is not Products fetch Task');
        setState(() {
          progressIndicatorVal = 1.0;
          showSplashScreen = false;
        });
      }
    } catch (e) {
      print('Inside loadIsolate Fn Error ');
      print(e);
      setState(() {
        progressIndicatorVal = 1.0;
        showSplashScreen = false;
      });
    }
  }

  static void isolateEntry(SendPort sendPort) async {
    print('Inside isolateEntry Fn ');
    try {
      ReceivePort port = ReceivePort();
      sendPort.send(port.sendPort);

      print('ReceivePort created,  now calling  handleAPIProductDataInsert');
//      handleAPIProductDataInsert(
////        taskId: FETCH_PRODUCTS_TASK_ID,
////        port: port,
//          );

      ///USED TO SEND THE ISOLATE PORT BACK TO THE PARENT ISOLATE,
      /// IN THIS CASE WE DON't WANT TO SEND ANY DATA TO PARENT ISOLATE SO COMMENTED THE BELOW CODE

//      sendPort.send(port.sendPort);
    } catch (e) {
      print('Error Inside isolateEntry Fn Error ');
      print(e);
    }
  }

//  static isolateEntry(SendPort sendPort) async {
//    print('Inside isolateEntry Fn Error ');
//    try {
//      ReceivePort port = ReceivePort();
//      print('ReceivePort created,  now calling  handleAPIProductDataInsert');
////      handleAPIProductDataInsert(
////        taskId: FETCH_PRODUCTS_TASK_ID,
////        port: port,
////      );
//
//      ///USED TO SEND THE ISOLATE PORT BACK TO THE PARENT ISOLATE,
//      /// IN THIS CASE WE DON't WANT TO SEND ANY DATA TO PARENT ISOLATE SO COMMENTED THE BELOW CODE
//
//      sendPort.send(port.sendPort);
//
////    await for (var msg in port) {
////      String data = msg[0];
////      SendPort replyPort = msg[1];
////
////      String url = data;
////
////      http.Response response = await http.get(url);
////
////      replyPort.send(json.decode(response.body));
////    }
//    } catch (e) {
//      print('Error Inside isolateEntry Fn Error ');
//      print(e);
//    }
//  }

  void _onClickEnable(enabled) {
    setState(() {
      _enabled = enabled;
    });
    if (enabled) {
      BackgroundFetch.start().then((int status) {
        print('[BackgroundFetch] start success: $status');
      }).catchError((e) {
        print('[BackgroundFetch] start FAILURE: $e');
      });
    } else {
      BackgroundFetch.stop().then((int status) {
        print('[BackgroundFetch] stop success: $status');
      });
    }
  }

  void _onClickStatus() async {
    int status = await BackgroundFetch.status;
    print('[BackgroundFetch] status: $status');
    setState(() {
      _status = status;
    });
  }

  void _onClickClear() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(EVENTS_KEY);
    setState(() {
      _events = [];
    });
  }

  ///BACKGROUND_FETCH NEW CHANGES END

  void updateProgress(double value) {
    setState(() {
      progressIndicatorVal = value;
    });
  }

  void handleAPICompanyDataInsert() {
    try {
      updateProgress(0.01);
      _companyDBHelper.fetchCompanies().then((apiCompaniesRes) => {
            print('fetchCompanies Response received from API'),
            if (apiCompaniesRes.length > 0)
              {
                this.updateProgress(0.3),
                print('Proceeding to insert company data into localDB'),
                _companyDBHelper.addCompanies(apiCompaniesRes).then(
                      (value) => {
                        print('Insert into Local Database is successful!'),
                        this.updateProgress(0.5),
//                        this.handleAPIProductDataInsert(),
                      },
                    ),
              }
            else
              {
                handleErrorCallback(
                  reason: 'No companies list received from api call',
                ),
              }
          });
    } catch (e) {
      print('Error inside handleAPICompanyDataInsert function ');
      print(e);
      handleErrorCallback(
        reason: 'Error occured while fetch or insert the company data',
      );
    }
  }

  String testFunction(String taskId) {
    return 'Success';
  }

//  ///THIS FUNCTION IS MAIN
//  static String handleAPIProductDataInsert(
////      {
////    taskId,
//////    ReceivePort port,
////  }
//      ) {
//    String taskId = FETCH_PRODUCTS_TASK_ID;
//    print('Inside GLobal handleAPIProductDataInsert Fn with taskId : $taskId ');
//    try {
//      if (true) {
//        print('Database Object Present for performing CRUD operations ');
//        _productDBHelper
//            .fetchProducts()
//            .then((apiProductsRes) => {
//                  print(
//                      'fetchProducts Response received from API In GLobal Function'),
//
//                  ///FROM HERE UNCOMMENT THE CODE
//                  if (apiProductsRes.length > 0)
//                    {
//                      DBProvider.db.database.then((db) => {
//                            db.delete(_productDBHelper.tableName).then(
//                                  (deleteRes) => {
//                                    print('deleteRes'),
//                                    print(deleteRes),
//                                    print(
//                                        'Proceeding to insert Product data into localDB'),
//                                    _productDBHelper
//                                        .addProducts(apiProductsRes)
//                                        .then(
//                                          (value) => {
//                                            print(
//                                                'Products Insert into Local Database is successful!'),
//                                            handleBackgroundFetchFinishStatic(
//                                                taskId: taskId),
////                                            if (port != null)
////                                              {
////                                                print(
////                                                    'Trying to close the Receiver port if the Isolate'),
//////                                                port.close(),
////                                              }
//                                            Future.value('Error Occured'),
//                                          },
//                                        ),
//                                  },
//                                ),
//                          }),
//                    }
//                  else
//                    {
//                      handleBackgroundFetchFinishStatic(taskId: taskId),
//                      handleErrorCallbackStatic(
//                        reason: 'No products list received from api call',
//                      ),
////                      Future.value('Error Occured'),
//                    }
//
//                  ///TILL HERE UNCOMMENT THE CODE
//                })
//            .catchError((e) {
//          print('Error from api ');
//          print(e);
//          handleBackgroundFetchFinishStatic(taskId: taskId);
////          return Future.value('Error Occured');
//          return 'Success';
//        });
//      } else {
//        print('Database Object not Present for performing CRUD operations ');
////        return Future.value('Test');
//        return "elseError";
//      }
//    } catch (e) {
//      print('Error inside handleAPIProductDataInsert function ');
//
//      print(e);
//      handleBackgroundFetchFinishStatic(taskId: taskId);
//      handleErrorCallbackStatic(
//        reason: 'Error Occured while API Product data fetch or insertion',
//      );
//
////      return Future.value('Error Occured');
//      return 'Error';
//    }
//  }

  void handleBackgroundFetchFinish({
    taskId,
  }) {
    try {
      print(
          'Inside handleBackgroundFetchFinish Fn for finishing the task for taskID: $taskId');
      if (taskId != null)
        BackgroundFetch.finish(taskId);
      else
        print('TaskID not present for closing Finishing the task');
    } catch (e) {
      print('Error inside handleBackgroundFetchFinish fn ');
      print(e);
    }
  }

  ///Handling the errors
  void handleErrorCallback({String reason}) {
    print('inside handleErrorCallback Error reason: $reason');
    print(
        'Due to error navigating to the Home Screen without inserting data into local tables');
    this.setState(() {
      progressIndicatorVal = 1.0;
      showSplashScreen = false;
    });
  }

  Widget getLoadingScreen() {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: Colors.white,
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(
                  value: progressIndicatorVal,
                  backgroundColor: Colors.cyan,
                  strokeWidth: 9.0,
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text('LOADING...${progressIndicatorVal * 100}%'),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> getEventsList() {
    List<Widget> _list = List<Widget>();
    if (_events.length > 0) {
      _events.forEach((singleEvent) {
        List<String> event = singleEvent.split("@");
        _list.add(
          InputDecorator(
              decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.only(left: 5.0, top: 5.0, bottom: 5.0),
                  labelStyle: TextStyle(color: Colors.blue, fontSize: 20.0),
                  labelText: "[${event[0].toString()}]"),
              child: new Text(event[1],
                  style: TextStyle(color: Colors.black, fontSize: 16.0))),
        );
      });
    }
    return _list;
  }

  @override
  Widget build(BuildContext context) {
    const EMPTY_TEXT = Center(
        child: Text(
            'Waiting for fetch events.  Simulate one.\n [Android] \$ ./scripts/simulate-fetch\n [iOS] XCode->Debug->Simulate Background Fetch'));

    return MaterialApp(
      home: showSplashScreen
          ? getLoadingScreen()
          : HomePage(
              apiProductsRes: _products,
            ),
    );
//    return new MaterialApp(
//      home: new Scaffold(
//        appBar: new AppBar(
//            title: const Text('BackgroundFetch Example',
//                style: TextStyle(color: Colors.black)),
//            backgroundColor: Colors.amberAccent,
//            brightness: Brightness.light,
//            actions: <Widget>[
//              Switch(value: _enabled, onChanged: _onClickEnable),
//            ]),
//        body: ListView(
//          children: <Widget>[
//            ///TIMERS ROWS START
//            Row(
//              children: <Widget>[
//                Expanded(
//                    child: Center(
//                        child: Padding(
//                  padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
//                  child: Text(
//                    'API DATA FETCH TIMING : $_productsFetchMSTime',
//                    style:
//                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
//                  ),
//                ))),
//              ],
//            ),
//            Row(
//              children: <Widget>[
//                Expanded(
//                    child: Center(
//                        child: Padding(
//                  padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
//                  child: Text(
//                    'LOCAL_DB DATA_DELETE TIMING : $_productsLocalDeleteMSTime',
//                    style:
//                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
//                  ),
//                ))),
//              ],
//            ),
//            Row(
//              children: <Widget>[
//                Expanded(
//                    child: Center(
//                        child: Padding(
//                  padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
//                  child: Text(
//                    'API DATA LOCAL INSERT TIMING : $_productsLocalInsertMSTime',
//                    style:
//                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
//                  ),
//                ))),
//              ],
//            ),
//            Row(
//              children: <Widget>[
//                Expanded(
//                    child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: RaisedButton(
//                    color: Colors.blue,
//                    onPressed: () {
//                      try {
//                        print('Starting BackgroundFetch on button click:');
//                        scheduleFetchProductTask();
//                      } catch (e) {
//                        print('Error while starting the BackgroundFetch ');
//                        print(e);
//                      }
//                    },
//                    child: Text('Sync Products in Background'),
//                  ),
//                ))
//              ],
//            ),
//            ...getEventsList(),
//          ],
//        ),
//        bottomNavigationBar: BottomAppBar(
//            child: Container(
//                padding: EdgeInsets.only(left: 5.0, right: 5.0),
//                child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      RaisedButton(
//                          onPressed: _onClickStatus,
//                          child: Text('Status: $_status')),
//                      RaisedButton(
//                          onPressed: _onClickClear, child: Text('Clear'))
//                    ]))),
//      ),
//    );
  }
}
